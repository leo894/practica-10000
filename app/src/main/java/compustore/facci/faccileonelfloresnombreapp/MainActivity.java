package compustore.facci.faccileonelfloresnombreapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText valortxt;
    Button enviarbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        valortxt = (EditText) findViewById(R.id.Cantidadhorastxt);
        enviarbtn = (Button) findViewById(R.id.calcularbtn);

        enviarbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inten = new  Intent(MainActivity.this , Main2Activity.class);

                Bundle bundle= new Bundle();

                bundle.putString("Sueldo",valortxt .getText().toString());

                inten.putExtras(bundle);
                startActivity(inten);

            }

        });

    }
}
