package compustore.facci.faccileonelfloresnombreapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView Mostrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Mostrar=(TextView) findViewById(R.id.totaltxt);

        String bundle=this.getIntent().getExtras().getString("Sueldo");

        if(bundle.length()<=160){

            Double gramos=Double.valueOf(bundle)*3;
            Mostrar.setText(gramos.toString());
            Log.e("text","Conversion Finalizada");


        }else{
            Double sueldo2=Double.valueOf(bundle) * 3.25;
            Mostrar.setText(sueldo2.toString());
            Log.d("text","Conversion Finalizada");
        }
    }
}
